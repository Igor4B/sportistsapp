﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SportistsApp.Data;
using SportistsApp.Models;

namespace SportistsApp.Controllers
{
    public class SportistsController : Controller
    {
        private readonly SportistsContext _context;

        public SportistsController(SportistsContext context)
        {
            _context = context;
        }

        // GET: Sportists
        public async Task<IActionResult> Index(string searchString, string sortOrder)
        {
            ViewData["NameSortParm"] = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            ViewData["CurrentFilter"] = searchString;

            var sportists = from s in _context.Sportists
                         select s;

            if (!string.IsNullOrEmpty(searchString))
            {
                sportists = sportists.Where(s => s.FirstName.Contains(searchString) || s.LastName.Contains(searchString));
            }
            switch (sortOrder)
            {
                case "name_desc":
                    sportists = sportists.OrderByDescending(s => s.FirstName);
                    break;
                default:
                    sportists = sportists.OrderBy(s => s.LastName);
                    break;
            }

            return View(await sportists.AsNoTracking().ToListAsync());
        }

        // GET: Sportists/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var sportists = await _context.Sportists
                .FirstOrDefaultAsync(m => m.Id == id);
            if (sportists == null)
            {
                return NotFound();
            }

            return View(sportists);
        }

        // GET: Sportists/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Sportists/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,FirstName,LastName,Sport,Ranking,SportistUrl,ImageUrl")] Sportists sportists)
        {
            if (ModelState.IsValid)
            {
                _context.Add(sportists);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(sportists);
        }

        // GET: Sportists/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var sportists = await _context.Sportists.FindAsync(id);
            if (sportists == null)
            {
                return NotFound();
            }
            return View(sportists);
        }

        // POST: Sportists/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,FirstName,LastName,Sport,Ranking,SportistUrl,ImageUrl")] Sportists sportists)
        {
            if (id != sportists.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(sportists);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!SportistsExists(sportists.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(sportists);
        }

        // GET: Sportists/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var sportists = await _context.Sportists
                .FirstOrDefaultAsync(m => m.Id == id);
            if (sportists == null)
            {
                return NotFound();
            }

            return View(sportists);
        }

        // POST: Sportists/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var sportists = await _context.Sportists.FindAsync(id);
            _context.Sportists.Remove(sportists);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool SportistsExists(int id)
        {
            return _context.Sportists.Any(e => e.Id == id);
        }
    }
}
