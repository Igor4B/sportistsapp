﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using SportistsApp.Models;

namespace SportistsApp.Data
{
    public class SportistsContext : DbContext
    {
        public SportistsContext (DbContextOptions<SportistsContext> options)
            : base(options)
        {
        }

        public DbSet<Sportists> Sportists { get; set; }
    }
}
