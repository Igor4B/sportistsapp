﻿namespace SportistsApp.Models
{
    public enum Sport
    {
        Football,
        Handball,
        Basketball,
        TableTennis,
        Swimming,
        Snooker,
        Tennis
    }
}
