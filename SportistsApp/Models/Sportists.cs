﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SportistsApp.Models
{
    public class Sportists
    {
        
        public int Id { get; set; }

        [Required]
        [StringLength(60, MinimumLength = 3)]

        public string FirstName { get; set; }
        [Required]
        [StringLength(60, MinimumLength = 3)]
        public string LastName { get; set; }
        
        [Required]
        public Sport Sport { get; set; }
        
        [Required]
        public int Ranking { get; set; }

        [Required]
        [DataType(DataType.Url)]
        [Display(Name = "Sportist Link")]
        public string SportistUrl { get; set; }

        [Required]
        [DataType(DataType.ImageUrl)]
        [Display(Name = "Picture")]
        public string ImageUrl { get; set; }
    }
}
